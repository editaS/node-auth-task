const Movie = require('../models/Movie')
const Utils = require('../utils')

module.exports.addMovie = async (req, res) => {
  const { title, description, cast } = req.body
  try {
    const movie = await Movie.create({
      title,
      description,
      cast
    })
    res.status(201).json(Utils.buildRes('success', movie))
  } catch (error) {
    console.error(error)
    res.status(400).json(Utils.buildRes('error', error))
  }
}

module.exports.getMovies = async (req, res) => {
  try {
    const movies = await Movie.find({})

    if (movies.length === 0) {
      return res.status(201).json(Utils.buildRes('success', 'There are no Movies in Database, please add some movies first'))
    }
    res.status(201).json(Utils.buildRes('success', movies))
  } catch (error) {
    console.error(error)
    res.status(400).json(Utils.buildRes('error', error))
  }
}

module.exports.queryMovies = async (req, res) => {
  const { title } = req.query
  try {
    const results = await Movie.find({ title })
    res.status(201).json(Utils.buildRes('success', results))
  } catch (error) {
    console.error(error)
    res.status(400).json(Utils.buildRes('error', error))
  }
}
