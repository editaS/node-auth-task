const jwt = require('jsonwebtoken')

const Utils = require('../utils')

// const sampleToken = jwt.sign('', 'process.env.TOKEN_SECRET')

const auth = async (req, res, next) => {
  const token = req.header('auth-token')
  if (!token) {
    return res.status(401).json(Utils.buildRes('error', 'Access Denied'))
  }

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET)
    req.authStatus = verified
    next()
  } catch (error) {
    res.status(400).json(Utils.buildRes('error', 'Invalid Token'))
  }
}

module.exports = auth
