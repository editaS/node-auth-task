const { Router } = require('express')

const MovieController = require('../controller/Movie')

const router = Router()

// @desc add a movie to Database
// @route POST /
router.post('/', MovieController.addMovie)

// @desc query movies on some criateria
// @route get /
router.get('/search', MovieController.queryMovies)

// @desc get the list of all movies in DB
// @route GET /
router.get('/', MovieController.getMovies)

module.exports = router
