const { Router } = require('express')

const router = Router()

// @desc Login/Landing page
// @route GET /
router.get('/', (req, res) => {
  res.render('home')
})

module.exports = router
