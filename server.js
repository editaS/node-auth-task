const path = require('path')
const express = require('express')
const dotenv = require('dotenv')
const morgan = require('morgan')

const connectDB = require('./config/db')
const baseRouter = require('./routes')
const moviesRouter = require('./routes/movies')
const auth = require('./middleware/Auth')

// Load config-
dotenv.config({ path: './config/config.env' })
connectDB()

const app = express()

// middlewares
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')))

// set the view engine to ejs
app.set('view engine', 'ejs')

// routes
app.use(baseRouter)
app.use('/api/movies', auth, moviesRouter)

const PORT = process.env.PORT || 3000
app.listen(PORT, console.log(`server runing in ${process.env.NODE_ENV} mode on port ${PORT}`))
